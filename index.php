<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	$messages = array();
	if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  $errors = array();
  $errors['field-name-1'] = !empty($_COOKIE['fio_error']);
  $errors['field-email'] = !empty($_COOKIE['email_error']);
  $errors['field-name-4'] = !empty($_COOKIE['sverh_error']);
  $errors['field-name-2'] = !empty($_COOKIE['bio_error']);
  $errors['check-1'] = !empty($_COOKIE['check_error']);
  if ($errors['field-name-1']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя, используя разрешенные символы (A-z, А-я,-).</div>';
  }
if ($errors['field-email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните e-mail, используя разрешенные символы (A-z,0-9,@,-,_,.)</div>';
  }
 
  if ($errors['field-name-4']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sverh_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберите сверхспособности.</div>';
  }
  if ($errors['field-name-2']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('bio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['check-1']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('check_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Поставьте галочку.</div>';
  }
   $values = array();
  $values['field-name-1'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['field-email'] = empty($_COOKIE['e-mail_value']) ? '' : $_COOKIE['e-mail_value'];
  $values['myselect'] = empty($_COOKIE['age_value']) ? '' : $_COOKIE['age_value'];
  $values['radio-group-1'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['radio-group-2'] = empty($_COOKIE['konch_value']) ? '' : $_COOKIE['konch_value'];
  $values['field-name-4'] = empty($_COOKIE['sverh_value']) ? '' : $_COOKIE['sverh_value'];
  $values['field-name-2'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
  $values['check-1'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
  include('index2.php');
}
else{
$errors = FALSE;
if (!preg_match('/^[a-zёа-я\s\-]+$/iu', $_POST['field-name-1'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['field-name-1'], time() + 12*30 * 24 * 60 * 60);
  }
  
  
if(!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $_POST['field-email'])){
	setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('e-mail_value', $_POST['field-email'], time() + 12*30 * 24 * 60 * 60);
  }
  setcookie('age_value', $_POST['myselect'], time() + 12*30 * 24 * 60 * 60);
	setcookie('sex_value', $_POST['radio-group-1'], time() + 12*30 * 24 * 60 * 60);
setcookie('konch_value', $_POST['radio-group-2'], time() + 12*30 * 24 * 60 * 60);

if (empty($_POST['field-name-4'])) {
  setcookie('sverh_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('sverh_value', implode("",$_POST['field-name-4']), time() + 12*30 * 24 * 60 * 60);
  }
if (empty($_POST['field-name-2'])) {
 setcookie('bio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('bio_value', $_POST['field-name-2'], time() + 12*30 * 24 * 60 * 60);
  }
if (empty($_POST['check-1'])) {
  setcookie('check_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('check_value', $_POST['check-1'], time() + 12*30 * 24 * 60 * 60);
  }
if ($errors) {
  header('Location: index.php');
    exit();
}
else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
	setcookie('email_error', '', 100000);
	setcookie('sverh_error', '', 100000);
	setcookie('bio_error', '', 100000);
	setcookie('check_error', '', 100000);
	 
  }
$user = 'u20383';
$pass = '9583134';
$db = new PDO('mysql:host=localhost;dbname=u20383', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

if ($db->connect_error) {
    die('Ошибка : ('. $db->connect_errno .') '. $db->connect_error);
}
$firstname=$_POST['field-name-1']; 
$email= $_POST['field-email'];
$age=$_POST['myselect'];
$sex=$_POST['radio-group-1'];
$kk=$_POST['radio-group-2'];
$ss=$_POST['field-name-4'];
$str=implode(' ',$ss);
$bio=$_POST['field-name-2'];
$sogl=$_POST['check-1'];
try {
  $stmt = $db->prepare("INSERT INTO users (fio, email, age, sex, kolvo_konechnostey, sverchsposobnosti, bio, soglasie) VALUES (?,?, ?,?,?,?,?,?);");
  $stmt -> execute(array($firstname,$email,$age,$sex,$kk,$str,$bio,$sogl));
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
setcookie('save', '1');
header('Location: index.php');
}
?>

